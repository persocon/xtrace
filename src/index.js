export { $ } from "katsu-curry"
export { callWithScopeWhen, callBinaryWithScopeWhen } from "./side-effect"
export {
  callWithScope,
  callWhen,
  call,
  callBinaryWithScope,
  callBinaryWhen,
  callBinary
} from "./derived"
export {
  traceWithScopeWhen,
  traceWhen,
  traceWithScope,
  trace,
  inspect
} from "./log"
export { always, I } from "./utils"
export { segment, segmentTrace } from "./segment"
export {
  sideEffect,
  taggedSideEffect,
  scopedSideEffect,
  scopedTrace
} from "./legacy"
