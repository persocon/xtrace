import { $ } from "katsu-curry"
import { callBinaryWithScopeWhen } from "./side-effect"
import { I, always } from "./utils"

// tracers
export const traceWithScopeWhen = callBinaryWithScopeWhen(console.log)
export const traceWithScope = traceWithScopeWhen(always)
export const inspect = traceWithScope
export const trace = inspect(I)
// no scope
export const traceWhen = callBinaryWithScopeWhen(console.log, $, I)
export const scopedTrace = inspect
/**
 * call console.log,
 * conditionally,
 * with a specific scope,
 * always return the last param
 * @method traceWithScopeWhen
 * @param {function} when - a binary predicate
 * @param {function} what - a lens, e.g. x => x.value
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { traceWithScopeWhen } from "xtrace"
 * import { prop } from "ramda"
 * // ... log as you calculate
 * pipe(
 *   traceWithScopeWhen(
 *     id => id === 200,
 *     prop('id'),
 *     'the 200 id item'
 *   ),
 *   calculate
 * )
 */

/**
 * call console.log,
 * with a specific scope,
 * always return the last param
 * @method inspect
 * @alias traceWithScope
 * @param {function} what - a lens, e.g. x => x.value
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { inspect } from "xtrace"
 * import { prop } from "ramda"
 * // ... log as you calculate
 * pipe(
 *   inspect(
 *     prop('id'),
 *     'the 200 id item'
 *   ),
 *   calculate
 * )
 */

/**
 * call console.log,
 * always return the last param
 * @method trace
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { trace } from "xtrace"
 * import { prop } from "ramda"
 * // ... log as you calculate
 * pipe(
 *   trace('data'),
 *   calculate
 * )
 */
