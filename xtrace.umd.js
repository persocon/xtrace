(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.xtrace = {}));
}(this, function (exports) { 'use strict';

  var PLACEHOLDER = "🍛";
  var $ = PLACEHOLDER;
  var bindInternal3 = function bindInternal3(func, thisContext) {
    return function (a, b, c) {
      return func.call(thisContext, a, b, c);
    };
  };
  var some$1 = function fastSome(subject, fn, thisContext) {
    var length = subject.length,
        iterator = thisContext !== undefined ? bindInternal3(fn, thisContext) : fn,
        i;
    for (i = 0; i < length; i++) {
      if (iterator(subject[i], i, subject)) {
        return true;
      }
    }
    return false;
  };
  var curry = function curry(fn) {
    var test = function test(x) {
      return x === PLACEHOLDER;
    };
    return function curried() {
      var arguments$1 = arguments;
      var argLength = arguments.length;
      var args = new Array(argLength);
      for (var i = 0; i < argLength; ++i) {
        args[i] = arguments$1[i];
      }
      var countNonPlaceholders = function countNonPlaceholders(toCount) {
        var count = toCount.length;
        while (!test(toCount[count])) {
          count--;
        }
        return count;
      };
      var length = some$1(args, test) ? countNonPlaceholders(args) : args.length;
      function saucy() {
        var arguments$1 = arguments;
        var arg2Length = arguments.length;
        var args2 = new Array(arg2Length);
        for (var j = 0; j < arg2Length; ++j) {
          args2[j] = arguments$1[j];
        }
        return curried.apply(this, args.map(function (y) {
          return test(y) && args2[0] ? args2.shift() : y;
        }).concat(args2));
      }
      return length >= fn.length ? fn.apply(this, args) : saucy;
    };
  };
  var innerpipe = function innerpipe(args) {
    return function (x) {
      var first = args[0];
      var rest = args.slice(1);
      var current = first(x);
      for (var a = 0; a < rest.length; a++) {
        current = rest[a](current);
      }
      return current;
    };
  };
  function pipe() {
    var arguments$1 = arguments;
    var argLength = arguments.length;
    var args = new Array(argLength);
    for (var i = 0; i < argLength; ++i) {
      args[i] = arguments$1[i];
    }
    return innerpipe(args);
  }
  var prop = curry(function (property, o) {
    return o && property && o[property];
  });
  var _keys = Object.keys;
  var keys = _keys;
  var propLength = prop("length");
  var objectLength = pipe(keys, propLength);
  var delegatee = curry(function (method, arg, x) {
    return x[method](arg);
  });
  var filter = delegatee("filter");
  function curryObjectN(arity, fn) {
    return function λcurryObjectN(args) {
      var joined = function joined(z) {
        return λcurryObjectN(Object.assign({}, args, z));
      };
      return args && Object.keys(args).length >= arity ? fn(args) : joined;
    };
  }

  var callWithScopeWhen = curry(function (effect, when, what, value) {
    var scope = what(value);
    if (when(scope)) effect(scope);
    return value;
  });
  var callBinaryWithScopeWhen = curry(function (effect, when, what, tag, value) {
    var scope = what(value);
    if (when(tag, scope)) effect(tag, scope);
    return value;
  });

  var always = function always() {
    return true;
  };
  var I$1 = function I(x) {
    return x;
  };

  var callWhen = callWithScopeWhen($, $, I$1);
  var call = callWithScopeWhen($, always, I$1);
  var callWithScope = callWithScopeWhen($, always);
  var callBinaryWhen = callBinaryWithScopeWhen($, $, I$1);
  var callBinaryWithScope = callBinaryWithScopeWhen($, always);
  var callBinary = callBinaryWithScopeWhen($, always, I$1);

  var traceWithScopeWhen = callBinaryWithScopeWhen(console.log);
  var traceWithScope = traceWithScopeWhen(always);
  var inspect = traceWithScope;
  var trace = inspect(I$1);
  var traceWhen = callBinaryWithScopeWhen(console.log, $, I$1);

  var segment = curryObjectN(3, function (_ref) {
    var _ref$what = _ref.what,
        what = _ref$what === void 0 ? I$1 : _ref$what,
        _ref$when = _ref.when,
        when = _ref$when === void 0 ? always : _ref$when,
        tag = _ref.tag,
        value = _ref.value,
        effect = _ref.effect;
    if (when(tag, what(value))) {
      effect(tag, what(value));
    }
    return value;
  });
  var segmentTrace = segment({
    effect: console.log
  });

  exports.$ = $;
  exports.callWithScopeWhen = callWithScopeWhen;
  exports.callBinaryWithScopeWhen = callBinaryWithScopeWhen;
  exports.callWithScope = callWithScope;
  exports.callWhen = callWhen;
  exports.call = call;
  exports.sideEffect = call;
  exports.callBinaryWithScope = callBinaryWithScope;
  exports.scopedSideEffect = callBinaryWithScope;
  exports.callBinaryWhen = callBinaryWhen;
  exports.callBinary = callBinary;
  exports.taggedSideEffect = callBinary;
  exports.traceWithScopeWhen = traceWithScopeWhen;
  exports.traceWhen = traceWhen;
  exports.traceWithScope = traceWithScope;
  exports.scopedTrace = traceWithScope;
  exports.trace = trace;
  exports.inspect = inspect;
  exports.always = always;
  exports.I = I$1;
  exports.segment = segment;
  exports.segmentTrace = segmentTrace;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
